/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gCOLUMN_ORDER_ID = 0;
const gCOLUMN_COMBO_SIZE = 1;
const gCOLUMN_PIZZA_TYPE = 2;
const gCOLUMN_DRINK = 3;
const gCOLUMN_PRICE = 4;
const gCOLUMN_FULLNAME = 5;
const gCOLUMN_PHONE = 6;
const gCOLUMN_STATUS = 7;
const gCOLUMN_ACTION = 8;
var gColName = ['orderId', 'kichCo', 'loaiPizza', 'idLoaiNuocUong', 'thanhTien', 'hoTen', 'soDienThoai', 'trangThai', 'detail'];

const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders"; //URL gọi ajax

var gOrdersObjs; // Tạo biến toàn cục chứa orders object array
var gOrdersObjFilter  // Tạo biến toàn cục chứa orders object array sau khi dc filter

// Tạo biến chứa trạng thái
var gStatus = {
  open: "Open",
  confirmed: "Confirmed",
  cancel: "Cancel"
};

// Tạo biến chứa loại pizza
var gPizzaType = {
  hawaii: "Hawaii",
  bacon: "Bacon",
  seafood: "Seafood"
};

//Tạo sẵn bảng bằng Datatable
var gTableOrders = $("#order-table").DataTable({
  // Khai báo các cột của datatable
  columns: [
    { "data": gColName[gCOLUMN_ORDER_ID] },
    { "data": gColName[gCOLUMN_COMBO_SIZE] },
    { "data": gColName[gCOLUMN_PIZZA_TYPE] },
    { "data": gColName[gCOLUMN_DRINK] },
    { "data": gColName[gCOLUMN_PRICE] },
    { "data": gColName[gCOLUMN_FULLNAME] },
    { "data": gColName[gCOLUMN_PHONE] },
    { "data": gColName[gCOLUMN_STATUS] },
    { "data": gColName[gCOLUMN_ACTION] }
  ],
  columnDefs: [
    {
      targets: gCOLUMN_ACTION,
      defaultContent: `<i class="fas fa-info-circle order-detail" title="Chi tiết order" style="cursor:pointer"></i>
                       <i class="fas fa-trash-alt delete-order" title="Xoá order" style="cursor:pointer"></i>
                      `
    }
  ]
});

//Tạo 2 biến ID và orderID của 1 order
var gId;
var gOrderId;
var gPizzaSize = ['S', 'M', 'L'];
var gDrinkList; // Biến chứa danh sách đồ uống
var gCombo = "none"; //Biến lưu trữ combo 

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
function loadOptionFilterStatus() { //Load option vào filter status
  for (var key in gStatus) {
    $('#order-status').append($('<option>', {
      value: key,
      text: gStatus[key]
    }));
  }
}

function loadOptionFilterPizzaType() { // Load option vào filter pizza type
  for (var key in gPizzaType) {
    $('#pizza-type').append($('<option>', {
      value: key,
      text: gPizzaType[key]
    }));
  }
}
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() { // Load table, filter options khi load trang
  callAjacGetAllOrders();
  loadOptionFilterStatus();
  loadOptionFilterPizzaType();
  loadDataToTable(gOrdersObjs);
}

function onBtnFilterOrderClick() {
  console.log("Nút filter order dc nhấn");
  //B1: get data đã 
  var vOrderStatusSelectInput = $("#order-status").val(); //Biến chứa select order status filter
  var vPizzaTypeSelectInput = $("#pizza-type").val(); //Biến chứa select pizza type filter

  //B2: validate data
  var vCheck = validateDataFilter(vOrderStatusSelectInput, vPizzaTypeSelectInput);
  //B3: xử lý data
  if (vCheck < 0) {
    console.log("Không có filter");
    //B4: hiển thị dữ liệu
    loadDataToTable(gOrdersObjs);
  } else {
    if (vCheck > 0) { // filter bằng order status và pizza type
      filterOrderByDataSelect(vOrderStatusSelectInput, vPizzaTypeSelectInput);
      //B4: hiển thị dữ liệu
      loadDataToTable(gOrdersObjFilter);
    }
  }
}

function validateDataFilter(paramOrderStatus, paramPizzaType) { //Kiểm tra input trước khi filter
  var vResult = 1;
  if (paramOrderStatus == 'all' && paramPizzaType == 'all') {
    vResult = -1;
  }
  return vResult;
}

function filterOrderByDataSelect(paramOrderStatus, paramPizzaType) { // filter order
  var vResult = gOrdersObjs.filter(function (gOrdersObj) {
    return (gOrdersObj.trangThai.toLowerCase() == paramOrderStatus.toLowerCase() || paramOrderStatus.toLowerCase() == "all") &&
      (gOrdersObj.loaiPizza.toLowerCase() == paramPizzaType.toLowerCase() || paramPizzaType.toLowerCase() == "all");
  });
  console.log(vResult);
  if (vResult.length > 0) { //nếu dữ liệu filter có thì thực hiện việc đổ data vào table sau 
    console.log("Tìm dc " + vResult.length + " order khớp sau khi filter!");
    gOrdersObjFilter = vResult;
  } else { // ko tìm dc user nào thì render hết data ra lại
    gOrdersObjFilter = [];
  }
}

// Tuỳ vào action là edit hay delete order thì sẽ gọi hàm này
function onBtnActionOrderClick(paramThisRow, paramAction) {
  console.log("Nút " + paramAction + " được nhấn");
  //Xác định thẻ tr là cha của nút được chọn
  var vThisRow = paramThisRow.closest('tr');
  //Lấy datatable row
  var vDatatableRow = gTableOrders.row(vThisRow);
  //Lấy data của dòng 
  var vOrderData = vDatatableRow.data();

  gId = vOrderData.id;
  gOrderId = vOrderData.orderId;
  console.log("Giá trị ID: " + gId);
  console.log("Giá trị orderID: " + gOrderId);
  if (paramAction == 'detail') {
    buildModalData(vOrderData); // Gọi hàm build detail order của modal
  } else if (paramAction == 'delete') {
    $("#delete-confirm-modal").modal('show'); // Show modal confirm delete order
  }
}

function onBtnNewOrderClick() {
  console.log("Nút thêm order dc nhấn");
  $('#create-drink') // Clear option của drink, r gọi api đổ vô lại mỗi khi call modal tạo order
    .find('option')
    .remove()
    .end()
    .append('<option value="none">Chọn 1 loại nước</option>');
  getDrinkList('create-drink'); // Lấy danh sách drink cho vào select modal
  clearCreateModalInput();
  $("#new-order-modal").modal('show');
}

function clearCreateModalInput() { //Clear dữ liệu của modal mỗi khi nut thêm order dc nhấn
  $("#create-size").val('none');
  $("#create-duongKinh").val('');
  $("#create-suon").val('');
  $("#create-salad").val('');
  $("#create-soLuongNuoc").val('');
  $("#create-thanhTien").val('');
  $("#create-type").val('none');
  $("#create-voucher").val('');
  $("#create-drink").val('none');
  $("#create-fullname").val('');
  $("#create-email").val('');
  $("#create-phone").val('');
  $("#create-diaChi").val('');
  $("#create-loiNhan").val('');
}

function onBtnCreateOrderClick() { //Nút crate order dc nhấn
  //B1: thu thập dữ liệu
  var vObjectRequest = {
    kichCo: $("#create-size").val(),
    duongKinh: gCombo.duongKinh,
    suon: gCombo.suon,
    salad: gCombo.salad,
    loaiPizza: $("#create-type").val(),
    idVourcher: $("#create-voucher").val().trim(),
    idLoaiNuocUong: $("#create-drink").val(),
    soLuongNuoc: gCombo.soLuongNuoc,
    hoTen: $("#create-fullname").val().trim(),
    thanhTien: gCombo.thanhTien,
    email: $("#create-email").val().trim(),
    soDienThoai: $("#create-phone").val().trim(),
    diaChi: $("#create-diaChi").val().trim(),
    loiNhan: $("#create-loiNhan").val().trim()
  }

  //B2: Kiểm tra dữ liệu
  var vCheck = validateCreateData(vObjectRequest);
  //B2.5: Kiểm tra voucher
  if(vObjectRequest.idVourcher != "") {
    var vFinalPrice = checkVoucherValid(vObjectRequest.idVourcher);
    vObjectRequest.thanhTien = vFinalPrice;
  }
  
  //B3: Xử lý dữ liệu
  if (vCheck) {
    callAjaxCreateNewOrder(vObjectRequest);
    //B4: hiển thị dữ liệu
    callAjacGetAllOrders();
  }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
function checkVoucherValid(paramVoucher) { // kiểm tra voucher có hợp lệ hay không để giảm giá
  var vVoucher;
  var vFinalPrice = gCombo.thanhTien;
  $.ajax({
    url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/"+paramVoucher,
    type: "GET",
    dataType: 'json',
    async: false,
    success: function (responseObject) {
      console.log("Kiểm tra thành cônggggggggggg");
      console.log(responseObject);
      vVoucher = responseObject;
      debugger
    },
    error: function (error) {
      console.assert(error.responseText);
      vVoucher = "none";
    }
  });
  if(vVoucher != "none") {
    vFinalPrice = vFinalPrice - (vVoucher.phanTramGiamGia * vFinalPrice / 100);
  }
  return vFinalPrice;
}

//Kích cỡ dùng thẻ select và add sự kiện change cho select,
//để khi chọn 1 loại kích cỡ nào thì tự động điền dữ liệu cho các ô thông tin ở trên
function getComboInfo(paramSize) {
  var vResult = true;
  if ($("#" + paramSize).val() == 'none') {
    alert("Chưa chọn pizza size");
    vResult = false;
    gCombo = "none";
  }
  if ($("#" + paramSize).val() == 'S') {
    gCombo = {
      duongKinh: "20",
      suon: "2",
      salad: "200",
      soLuongNuoc: "2",
      thanhTien: "150000"
    }
  }
  if ($("#" + paramSize).val() == 'M') {
    gCombo = {
      duongKinh: "25",
      suon: "4",
      salad: "300",
      soLuongNuoc: "3",
      thanhTien: "200000"
    }
  }
  if ($("#" + paramSize).val() == 'L') {
    gCombo = {
      duongKinh: "30",
      suon: "8",
      salad: "500",
      soLuongNuoc: "4",
      thanhTien: "250000"
    }
  }
  fillComboInput(gCombo);
  return vResult;
}

function fillComboInput(paramCombo) { // khi chọn 1 size pizza thì tự điền combo vào
  if (paramCombo != "none") {
    $("#create-duongKinh").val(paramCombo.duongKinh);
    $("#create-suon").val(paramCombo.suon);
    $("#create-salad").val(paramCombo.salad);
    $("#create-soLuongNuoc").val(paramCombo.soLuongNuoc);
    $("#create-thanhTien").val(paramCombo.thanhTien);
  }
}

function validateCreateData(paramCreateDataInput) { //Kiểm tra dữ liệu input khi tạo mới 1 order
  var vResult = true;
  if (paramCreateDataInput.kichCo == 'none') {
    vResult = false;
    alert("Chưa chọn kích cỡ");
  }
  if (paramCreateDataInput.loaiPizza == 'none') {
    vResult = false;
    alert("Chưa chọn loại pizza");
  }
  if (paramCreateDataInput.idLoaiNuocUong == 'none') {
    vResult = false;
    alert("Chưa chọn loại nước uống");
  }
  if (paramCreateDataInput.hoTen == '') {
    vResult = false;
    alert("Chưa nhập họ tên");
  }
  if (paramCreateDataInput.email == '') {
    vResult = false;
    alert("Chưa nhập email");
  }
  if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(paramCreateDataInput.email)) {
    vResult = false;
    alert("Email không đúng format");
  }
  if (paramCreateDataInput.soDienThoai == '') {
    vResult = false;
    alert("Chưa nhập số điện thoại");
  }
  if (paramCreateDataInput.diaChi == '') {
    vResult = false;
    alert("Chưa nhập địa chỉ");
  }
  return vResult;
}

function validateEmail(paramEmail) { //Kiểm tra format email
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(paramEmail)) {
    return true
  }
  alert("You have entered an invalid email address!")
  return false
}

function callAjaxCreateNewOrder(paramObjectRequest) { // gọi server tạo mới 1 order
  $.ajax({
    url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
    type: "POST",
    dataType: 'json',
    data: JSON.stringify(paramObjectRequest),
    contentType: "application/json;charset=UTF-8",
    async: false,
    success: function (responseObject) {
      console.log("Tạo mới 1 order thành cônggggggggggg");
      console.log(responseObject);
      $("#new-order-modal").modal('hide');
      $("#order-action-success-text").val("Đã tạo thành công order có OrderId: " + responseObject.orderId);
      $("#order-action-success-modal").modal('show');
    },
    error: function (error) {
      console.assert(error.responseText);
    }
  });
}

function buildModalData(paramCurrentData) { // Render body của modal chứa order detail
  var vModalBody = $("#order-detail-modal-body"); // truy xuất phần tử modal body order detail
  // Trước khi build thì clear hết data cũ đi
  vModalBody.empty();
  //Loop qua object hiện tại, đếm độ dài object r đổ data vào
  for (var bI = 1; bI < Object.keys(paramCurrentData).length; bI++) {
    var vLabel = Object.keys(paramCurrentData)[bI];
    var vData = paramCurrentData[Object.keys(paramCurrentData)[bI]];
    vModalBody.append('<div class="row form-group">' +
      '<div class="col-sm-4">' +
      '<label for="' + vLabel + '">' + vLabel + '</label>' +
      '</div>' +
      '<div class="col-sm-8">' +
      '<input class="form-control" readonly type="text" name="' + vLabel + '" id="' + vLabel + '">' +
      '</div>' +
      '</div>');
  }
  fillInputTextModalWithData(paramCurrentData);
}

function fillInputTextModalWithData(paramCurrentData) { // Fill data vào input của modal order detail
  for (var bI = 1; bI < Object.keys(paramCurrentData).length; bI++) {
    var vId = Object.keys(paramCurrentData)[bI];
    var vValue = paramCurrentData[Object.keys(paramCurrentData)[bI]];
    $("#" + vId).val(vValue);
  }
  $("#order-detail-modal").modal('show'); // Hiện model lên sau khi render xong
}

function getPizzaSizeList(paramPizza, paramSelectId) { // lấy danh sách pizza size
  $.each(paramPizza, function (i, item) {
    $("#" + paramSelectId).append($('<option>', {
      text: item,
      value: item.toLowerCase()
    }))
  });
}

function getDrinkList(paramSelectId) { //lấy danh sách đồ uống
  $.ajax({
    url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
    type: "GET",
    dataType: 'json',
    async: false,
    success: function (responseObject) {
      gDrinkList = responseObject;
      console.log(gDrinkList);
    },
    error: function (error) {
      console.assert(error.responseText);
    }
  });

  $.each(gDrinkList, function (i, item) { //Sau khi gọi ajax xong thì đổ data vào select drink
    $("#" + paramSelectId).append($('<option>', {
      text: item.tenNuocUong,
      value: item.maNuocUong
    }))
  });
}

function callAjacGetAllOrders() { // gọi server lấy dữ liệu order
  $.ajax({
    url: gBASE_URL,
    type: "GET",
    dataType: 'json',
    async: false,
    success: function (responseObjects) {
      gOrdersObjs = responseObjects.filter(function (responseObject) { //filter các order mà pizza type và status bị null
        return (responseObject.trangThai.toLowerCase() != null) &&
               (responseObject.loaiPizza.toLowerCase() != null);
      });
      console.log(gOrdersObjs);
      loadDataToTable(gOrdersObjs);
    },
    error: function (error) {
      console.assert(error.responseText);
    }
  });
}

function loadDataToTable(paramOrderObj) { // đổ dữ liệu vào bảng
  //Xóa toàn bộ dữ liệu đang có của bảng
  gTableOrders.clear();
  //Cập nhật data cho bảng 
  gTableOrders.rows.add(paramOrderObj);
  //Cập nhật lại giao diện hiển thị bảng
  gTableOrders.draw();
}

function onBtnModalOrderClick(paramActionModal) { //hàm gọi confirm hoặc cancel 1 order, tuỳ vào param truyền vào
  console.log("Nút " + paramActionModal + " được nhấn");
  console.log("Giá trị ID: " + gId);
  //B1: đọc dữ liệu
  //input là id của order = gId (biến toàn cục thay đổi mỗi khi nhấn nút chi tiết order hiện modal)

  //B2: validate, bỏ qua bước này do lấy dc id của order là chính xác

  //B3: gọi server
  var vObjectRequest = {
    trangThai: paramActionModal //3: trang thai open, confirmed, cancel tùy tình huống
  }
  $.ajax({
    url: gBASE_URL + "/" + gId,
    type: "PUT",
    dataType: 'json',
    data: JSON.stringify(vObjectRequest),
    contentType: "application/json;charset=UTF-8",
    async: false,
    success: function (response) {
      console.log("Update thành côngggggggggggg");
      console.log("Trạng thái hiện giờ là: " + response.trangThai);
      console.log(response);
      $("#order-detail-modal").modal('hide'); //Ẩn modal sau khi đã update xong
      $("#order-action-success-text").val("Đã " + paramActionModal + " order");
      $("#order-action-success-modal").modal('show');
      callAjacGetAllOrders(); // Load lại table để xem thay đổi
    },
    error: function (error) {
      console.assert(error.responseText);
    }
  });
}

function onBtnModalDeleteOrderClick() { //Xoá 1 order
  $.ajax({
    url: gBASE_URL + "/" + gId,
    type: "DELETE",
    dataType: 'json',
    contentType: "application/json;charset=UTF-8",
    async: false,
    success: function (response) {
      console.log("Xoá thành côngggggggggggg");
      console.log(response);
      $("#delete-confirm-modal").modal('hide'); //Ẩn modal sau khi đã update xong
      $("#order-action-success-text").val("Đã xoá order");
      $("#order-action-success-modal").modal('show');
      callAjacGetAllOrders(); // Load lại table để xem thay đổi
    },
    error: function (error) {
      console.assert(error.responseText);
    }
  });
}

// Vùng xử lý và gọi hàm tương ứng các sự kiện onclick, onchange
$(document).ready(function () {
  onPageLoading();

  //Khi ấn, lấy dữ liệu của Row và ghi vào console
  $("#order-table").on('click', 'td .order-detail', function () {
    onBtnActionOrderClick(this, 'detail');
  });

  //Khi ấn, lấy dữ liệu của Row và ghi vào console
  $("#order-table").on('click', 'td .delete-order', function () {
    onBtnActionOrderClick(this, 'delete');
  });

  // Khi ấn nút delete order
  $("#btn-confirm-delete-order").on('click', function () {
    onBtnModalDeleteOrderClick();
  });

  // Khi ấn nút filter order table
  $("#btn-filter").on('click', onBtnFilterOrderClick);

  // Khi ấn nút confirm order
  $("#btn-modal-confirm-order").on('click', function () {
    onBtnModalOrderClick('confirmed');
  });

  // Khi ấn nút cancel order
  $("#btn-modal-cancel-order").on('click', function () {
    onBtnModalOrderClick('cancel');
  });

  //Nút thêm mới 1 order dc nhấn
  $("#btn-new-order").on('click', function () {
    onBtnNewOrderClick();
  });

  //Khi chọn kichCo pizza thì đổ data combo vào các text của combo
  $("#create-size").on('change', function () {
    getComboInfo(this.id);
  })

  $("#btn-modal-create-order").on('click', function () {
    onBtnCreateOrderClick();
  });

});